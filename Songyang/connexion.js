import React from 'react';

import { SafeAreaView,
 ImageBackground,
 Dimensions,
 StyleSheet,
// Button,
TouchableOpacity,
 Image,
 View
 } from 'react-native';

import { Divider, Icon, Layout, Text, TopNavigation, TopNavigationAction, Input,
 Button,
 BottomNavigation, BottomNavigationTab
 } from '@ui-kitten/components';

import { AppNavigator_bottom } from './bottomNavigation';

const BackIcon = (props) => (
  <Icon {...props} name='arrow-back' />
);

export const ConnexionScreen = ({ navigation,state }) => {

  const navigateBack = () => {
    navigation.goBack();
  };

  const BackAction = () => (
    <TopNavigationAction icon={BackIcon} onPress={navigateBack}/>
  );

  let MainHeight = Dimensions.get('window').height;
  let MainWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  input: {
//    flex: 1,
    margin: 5,
    marginLeft: 50,
    marginRight: 50,
    borderRadius:20
  },
  button: {
      alignItems:"center",
      justifyContent:"center",
      backgroundColor: "lightgreen",
      //padding: 20,
      borderRadius: 15,
      height:30,
      width:90
    },
});



const [value, setValue] = React.useState('');  //input

const onPress = () => {navigation.navigate('Connexion') };


const PersonIcon = (props) => (
  <Icon {...props} name='person-outline'/>
);

const BellIcon = (props) => (
  <Icon {...props} name='bell-outline'/>
);

const EmailIcon = (props) => (
  <Icon {...props} name='email-outline'/>
);

const [selectedIndex, setSelectedIndex] = React.useState(0);

  const navigateDetails = () => {
    navigation.navigate('Details');
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>




      <TopNavigation title='MyApp' alignment='center' accessoryLeft={BackAction} backgroundColor='rgba(52, 52, 52, 0.8)' />




      <Divider/>

      <Layout style={{ flex: 1,  alignItems: 'center'}}  level='1'>
        <Text category='h1'>Inscrivez-vous</Text>
              <Input
                style={styles.input}
                value={value}
                placeholder='E-mail'
                onChangeText={nextValue => setValue(nextValue)}
//                style={{ flex: 0.8, justifyContent: 'center', alignItems: 'center' }}
              />

               <Input
                              style={styles.input}
                              value={value}
                              placeholder='Mot de passe'
                              onChangeText={nextValue => setValue(nextValue)}
               />

               <Input
                                             style={styles.input}
                                             value={value}
                                             placeholder='Confirmation du Mot de passe'
                                             onChangeText={nextValue => setValue(nextValue)}
               />

                <TouchableOpacity onPress={onPress} style={styles.button}>
                  <Text style={styles.buttonText}>S'inscrire</Text>
                </TouchableOpacity>


      </Layout>

      <Divider/>

      <Layout style={{ flex: 1,  alignItems: 'center' }}>
              <Text category='h1'>Connextion Rapide</Text>
              <Layout style={{marginTop:10, flexDirection: 'row', justifyContent:"flex-start",flexWrap:'wrap', height:MainHeight*0.2, width:MainWidth*0.8}} >
                <Image
                    style={{   height:50,width:50,marginLeft:70, marginRight:100}}
                   source = {require('./images/facebook.png')}

                />
                <Image
                    style={{  height:50,width:50 }}
                    source = {require('./images/google.png')}

                />
              </Layout>
              <Text>Vous avez deja un compte?</Text>
              <Text style={{color:"green",  textDecorationLine:'underline',  borderBottomColor:"green", borderBottomWidth:2 }}>Connectez-vous</Text>

      </Layout>

      <Divider/>



    </SafeAreaView>
  );
};