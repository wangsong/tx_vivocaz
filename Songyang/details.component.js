import React from 'react';
import { SafeAreaView, View,  StyleSheet, Dimensions, Image, TouchableOpacity } from 'react-native';
import { Divider, Icon, Layout, Text, TopNavigation, TopNavigationAction } from '@ui-kitten/components';
import  ViewPagerAndroid  from '@react-native-community/viewpager'
import Swiper from 'react-native-swiper'

const BackIcon = (props) => (
  <Icon {...props} name='arrow-back' />
);

const { width, height } = Dimensions.get('window');//获取手机的宽和高


export const DetailsScreen = ({ navigation }) => {

  const navigateBack = () => {
    navigation.goBack();
  };

  const BackAction = () => (
    <TopNavigationAction  icon={BackIcon} onPress={navigateBack}/>
  );

const styles =StyleSheet.create( {
    wrapper: {

    },
    container: {
        //flex: 1,//必写
//        display: block,
        alignItems: "center",
        justifyContent: "center",

        height: height*0.3,
        width: width*0.8,
        paddingLeft: 10
//        backgroundColor: "red" ,
    },
    image: {
        width: width*0.9,//等于width:width
        height: height*0.2,
    },
    button: {
          alignItems:"center",
          justifyContent:"center",
          backgroundColor: "lightgreen",
          //padding: 20,
          borderRadius: 15,
          height:30,
          width:90
        },
});

const onPress = () => {navigation.navigate('Details') };



  return (
    <SafeAreaView style={{ flex: 1, alignItems: "center", justifyContent: "center", backgroundColor:"white"}}>
        <View style={{marginTop:0, left:-width*0.45 }}>
        <BackAction />
        </View>

      <Divider/>
      <Layout style={{ flex: 0.4,  alignItems: 'center' }}>
        <Text style={{fontWeight: "bold"}}>
            Vivocaz vous conduit vers le vehicule ideal
        </Text>
        <Text style={{fontWeight: "bold", top:30}}>
             ——Recherche Guidee——
        </Text>
         <Text style={{top:60}}>
                Explication du contexte
        </Text>
      </Layout>

    <Layout style={{ flex: 1,     alignItems: 'center' }}>

        <Layout style={styles.container}>
                        <Swiper style={styles.wrapper}
                                showsButtons={true}       //为false时不显示控制按钮
                                paginationStyle={{      //小圆点位置
                                    bottom: 10
                                }}
                                loop={false}        //如果设置为false，那么滑动到最后一张时，再次滑动将不会滑到第一张图片。
                                autoplay={true}          //自动轮播
                                autoplayTimeout={20}      //每隔2秒切换
                        >
                            <Text         alignItems= "center"
                                          justifyContent= "center"  >

                            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.111"
                            </Text>

                            <Text>
                             "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.111"
                            </Text>

                            <Text>
                            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.111"
                            </Text>

                        </Swiper>
                    </Layout>


                    <TouchableOpacity onPress={onPress} style={styles.button}>
                         <Text style={styles.buttonText}>Suivant</Text>
                    </TouchableOpacity>
            </Layout>

    </SafeAreaView>
  );
};