import React from 'react';
import { NavigationContainer,DefaultTheme } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import { HomeScreen } from './home.component';
import { DetailsScreen } from './details.component';
import { ConnexionScreen } from './connexion';
import { MenuScreen } from './menu';

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { BottomNavigation, BottomNavigationTab, Layout, Text, Icon } from '@ui-kitten/components';

//import { MaterialIcons } from "@expo/vector-icons";

//const { Navigator, Screen } = createStackNavigator();
//const Stack = createNativeStackNavigator();
const { Navigator, Screen } = createBottomTabNavigator();

const PersonIcon = (props) => (
  <Icon {...props} name='person-outline'/>
);

const BellIcon = (props) => (
  <Icon {...props} name='bell-outline'/>
);

const EmailIcon = (props) => (
  <Icon {...props} name='email-outline'/>
);


const MyTheme = {
  dark: false,
  colors: {
    primary: 'rgb(255, 45, 85)',
    background: 'rgb(242, 242, 242)',
    card: 'rgb(255, 255, 255)',
    text: 'rgb(28, 28, 30)',
    border: 'rgb(199, 199, 204)',
    notification: 'rgb(255, 69, 58)',
  },
};



/*
const UsersScreen = () => (
  <Layout style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
    <Text category='h1'>USERS</Text>
  </Layout>
);

const OrdersScreen = () => (
  <Layout style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
    <Text category='h1'>ORDERS</Text>
  </Layout>
);
*/


/*
const HomeNavigator = () => (

  <Stack.Navigator screenOptions={{headerShown: false}}>
    <Stack.Screen name='Home' component={HomeScreen}/>
    <Stack.Screen name='Details' component={DetailsScreen}/>
    <Stack.Screen name='Connexion' component={ConnexionScreen}/>

  </Stack.Navigator>
);
*/

const BottomTabBar = ({ navigation, state }) => (
  <BottomNavigation
//    appearance="noIndicator"
//    colors="pink"
    selectedIndex={state.index}
    onSelect={index => navigation.navigate(state.routeNames[index])}>



    <BottomNavigationTab title='Home' icon={PersonIcon} colors="pink" backgroundColor="pink"/>
    <BottomNavigationTab title='Details' icon={PersonIcon}/>

    <BottomNavigationTab title='Connexion' icon={PersonIcon}/>
  </BottomNavigation>
);

const TabNavigator = () => (

  <Navigator tabBar={props => <BottomTabBar {...props}    />}  initialRoute={{type: 'Home'}} >
    <Screen name='Home' component={HomeScreen} backgroundColor="pink" />
    <Screen name='Details' component={DetailsScreen}  style={{backgroundColor:"pink"  }}  />

    <Screen name=' ' component={ConnexionScreen}/>
  </Navigator>
);

/*
export  default  class  AppNavigator extends Component{

    render(){
       return (
             <NavigationContainer>
//                <HomeNavigator/>
                      <Stack.Navigator screenOptions={{headerShown: false}}>
                        <Stack.Screen name='Home' component={HomeScreen}/>
                        <Stack.Screen name='Details' component={DetailsScreen}/>
                      </Stack.Navigator>

              </NavigationContainer>

       )

    }

}
*/

/*
export const AppNavigator = () => (
  <NavigationContainer>
    <HomeNavigator/>
  </NavigationContainer>
);
*/

export const AppNavigator = () => (
  <NavigationContainer >
    <TabNavigator/>
  </NavigationContainer>
);