import React from 'react';
import { SafeAreaView, ScrollView, Image, Dimensions } from 'react-native';
import { Button, Divider, Layout, TopNavigation, Text } from '@ui-kitten/components';

  let MainHeight = Dimensions.get('window').height;
  let MainWidth = Dimensions.get('window').width;


export const HomeScreen = ({ navigation }) => {

  const navigateDetails = () => {
    navigation.navigate('Details');
  };

    const navigateConnexion = () => {
      navigation.navigate('Connexion');
    };

  return (
    <SafeAreaView style={{ flex: 1 }}>

      <Divider/>
      <Layout style={{ flex: 1, alignItems: 'center' }}>

        <Layout style={{ flex: 0.55, alignItems: 'center', backgroundColor:"gray", padding:10 }}  level='1' >
            <Text style={{color:"white" }}>Commencer une nouvelle recherche guidee maintenant!</Text>
        </Layout>
         <Divider/>

         <Layout style={{ flex: 1,   backgroundColor:"white"}}  >
         <Text style={{fontWeight: "bold" , left:5}}>Nos dernieres suggestions</Text>
          <ScrollView horizontal={true}>


                   <Image style={{marginRight:10}} source={require("./images/renault1.jpg")} />
                   <Image style={{marginRight:10}} source={require("./images/renault2.jpg")} />
                   <Image style={{marginRight:10}} source={require("./images/renault3.jpg")} />
                   <Image style={{marginRight:10}} source={require("./images/renault4.jpg")} />

           </ScrollView>
         </Layout>
         <Divider/>

         <Layout style={{ flex: 1 }}  >
         <Text style={{fontWeight: "bold", left:5}}>Vos selections</Text>
            <ScrollView horizontal={true}>

            <Image style={{marginRight:10}} source={require("./images/renault1.jpg")} />
            <Image style={{marginRight:10}} source={require("./images/renault2.jpg")} />
            <Image style={{marginRight:10}} source={require("./images/renault3.jpg")} />
            <Image style={{marginRight:10}} source={require("./images/renault4.jpg")} />
        </ScrollView>

         </Layout>


      </Layout>

    </SafeAreaView>
  );
};